package edu.fatec.solid;

public enum Cargo {
	DBA(new RegraQuinzeOuVinteCinco()),
	DESENVOLVEDOR(new RegraDezOuVinte()),
	TESTER(new RegraQuinzeOuVinteCinco());

	RegraPorcentagem regraPorcentagem;

	Cargo(RegraPorcentagem r) {
		regraPorcentagem = r;
	}

	public RegraPorcentagem getRegraPorcentagem() {
		return regraPorcentagem;
	}
}

package edu.fatec.solid;

public interface RegraPorcentagem {
	double calcular(Funcionario funcionario);
}

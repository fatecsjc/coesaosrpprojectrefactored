package edu.fatec.solid;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CargoTest {

    private Funcionario funcionario;

    @Before
    public void init() {

        funcionario = new Funcionario();
    }

    @Test
    public void testaCargoDBA() {

        funcionario.setCargo(Cargo.DBA);
        assertEquals(Cargo.DBA, funcionario.getCargo());
    }

    @Test
    public void testaCargoDesenvolvedor() {

        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        assertEquals(Cargo.DESENVOLVEDOR, funcionario.getCargo());
    }

    @Test
    public void testaCargoTester() {

        funcionario.setCargo(Cargo.TESTER);
        assertEquals(Cargo.TESTER, funcionario.getCargo());
    }
}

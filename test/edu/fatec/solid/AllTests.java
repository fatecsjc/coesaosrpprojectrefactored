package edu.fatec.solid;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculadoraSalarioTest.class, CargoTest.class, FuncionarioTest.class, RegraDezOuVinteTest.class, RegraQuinzeOuVinteCincoTest.class})
public class AllTests {
}

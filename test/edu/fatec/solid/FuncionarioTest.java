package edu.fatec.solid;

import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class FuncionarioTest {

    private Funcionario funcionario;
    private DateFormat dataParser;
    private Calendar calendar;

    @Before
    public void init() {

        funcionario = new Funcionario();
    }

    @Before
    public void setUp() throws Exception{
        dataParser = new SimpleDateFormat("MM dd yyyy");
    }

    @Test
    public void testaNomeFuncionario() {

        funcionario.setNome("Robson");
        assertEquals("Robson", funcionario.getNome());
    }

    @Test
    public void testaSalarioBaseFuncionario() {
        funcionario.setSalarioBase(5000.00);
        assertEquals(5000.00, funcionario.getSalarioBase(), 0.01);
    }

    @Test
    public void testaCargoFuncionario() {

        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        assertEquals(Cargo.DESENVOLVEDOR, funcionario.getCargo());
    }

    @Test
    public void testaIdFuncionario(){

        funcionario.setId(1);
        assertEquals(1, funcionario.getId());
    }
}

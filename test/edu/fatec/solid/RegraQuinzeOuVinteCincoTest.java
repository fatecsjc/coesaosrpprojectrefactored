package edu.fatec.solid;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class RegraQuinzeOuVinteCincoTest {

    private Funcionario funcionario;
    private RegraQuinzeOuVinteCinco regraQuinzeOuVinteCinco;

    @Before
    public void init() {

        funcionario = new Funcionario();
        regraQuinzeOuVinteCinco = new RegraQuinzeOuVinteCinco();
    }

    @Test
    public void testaRegraQuinzeOuVinteCincoSalarioBaseMaiorQueDoisMilParaDBA() {

        funcionario.setCargo(Cargo.DBA);
        funcionario.setSalarioBase(5000.00);

        assertEquals(3750.00, regraQuinzeOuVinteCinco.calcular(funcionario));
    }

    @Test
    public void testaRegraQuinzeOuVinteCincoSalarioBaseMenorOuIgualQueDoisMilParaDBA() {

        funcionario.setCargo(Cargo.DBA);
        funcionario.setSalarioBase(2000.00);

        assertEquals(1700.00, regraQuinzeOuVinteCinco.calcular(funcionario));
    }

    @Test
    public void testaRegraQuinzeOuVinteCincoSalarioBaseMaiorQueDoisMilParaTester() {

        funcionario.setCargo(Cargo.TESTER);
        funcionario.setSalarioBase(3000.00);

        assertEquals(2250.00, regraQuinzeOuVinteCinco.calcular(funcionario));
    }

    @Test
    public void testaRegraQuinzeOuVinteCincoSalarioBaseMenorOuIgualQueDoisMilParaTester() {

        funcionario.setCargo(Cargo.TESTER);
        funcionario.setSalarioBase(1500.00);
        assertEquals(1275.00, regraQuinzeOuVinteCinco.calcular(funcionario));
    }
}

package edu.fatec.solid;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;


public class CalculadoraSalarioTest {

    private Funcionario funcionario;
    private CalculadoraSalario calculadoraSalario;

    @Before
    public void init() {
        funcionario = new Funcionario();
        calculadoraSalario = new CalculadoraSalario();
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMaiorQueDoisMilParaDBA() {
        funcionario.setCargo(Cargo.DBA);
        funcionario.setSalarioBase(5000.00);

        assertEquals(3750.00, calculadoraSalario.calcula(funcionario));
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMenorOuIgualQueDoisMilParaDBA() {

        funcionario.setCargo(Cargo.DBA);
        funcionario.setSalarioBase(2000.00);

        assertEquals(1700.00, calculadoraSalario.calcula(funcionario));
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMaiorQueDoisMilParaTester() {

        funcionario.setCargo(Cargo.TESTER);
        funcionario.setSalarioBase(3000.00);

        assertEquals(2250.00, calculadoraSalario.calcula(funcionario));
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMenorOuIgualQueDoisMilParaTester() {

        funcionario.setCargo(Cargo.TESTER);
        funcionario.setSalarioBase(1500.00);

        assertEquals(1275.00, calculadoraSalario.calcula(funcionario));
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMaiorQueDoisMilParaDesenvolvedor() {

        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        funcionario.setSalarioBase(4000.00);

        assertEquals(3200.00, calculadoraSalario.calcula(funcionario));
    }

    @Test
    public void testaCalculadoraSalarioSalarioBaseMenorOuIgualQueDoisMilParaDesenvolvedor() {

        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        funcionario.setSalarioBase(2500.00);

        assertEquals(2250.00, calculadoraSalario.calcula(funcionario));
    }
}

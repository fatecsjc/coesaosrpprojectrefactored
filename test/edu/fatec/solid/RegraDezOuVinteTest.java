package edu.fatec.solid;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class RegraDezOuVinteTest {

    private Funcionario funcionario;
    private RegraDezOuVinte regraDezOuVinte;

    @Before
    public void init() {

        funcionario = new Funcionario();
        regraDezOuVinte = new RegraDezOuVinte();
    }

    @Test
    public void testaRegraDezOuVinteSalarioBaseMaiorQueTresMilParaDesenvolvedor() {
        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        funcionario.setSalarioBase(4000.00);
        assertEquals(3200.00, regraDezOuVinte.calcular(funcionario));
    }

    @Test
    public void testaRegraDezOuVinteSalarioBaseMenorOuIgualQueTresMilParaDesenvolvedor() {

        funcionario.setCargo(Cargo.DESENVOLVEDOR);
        funcionario.setSalarioBase(2500.00);

        assertEquals(2250.00, regraDezOuVinte.calcular(funcionario));
    }
}
